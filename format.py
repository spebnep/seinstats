import os, re
import json


print("FORMAT.PY")
print("==========")
print()
print("For use in Seinfeld Data Project 001")
print()
here = os.path.dirname(os.path.abspath(__file__))

unfd = here + '/scripts/unformatted/'
pard = here + '/scripts/deparenned/'
ford = here + '/scripts/formatted/'
for unf in os.listdir(unfd):
	if unf == ".DS_Store":
		pass
	else:
		# Get rid of parentheses
		print("Opened " + unf + " for de-parenning")
		with open(unfd + unf, 'r') as f:
			dplin = []
			lines = f.readlines()
			for l in lines:
				if l == '\n':
					pass
				else:
					l = re.sub(r'\([^)]*\) ', '', l)
					dplin.append(l)
			t = "".join(dplin)
			with open(pard + unf[:-2] + 'dp', 'w+') as ff:
				ff.write(t)
				os.remove(unfd + unf)
				print("Successfully de-parenned " + unf)

for par in os.listdir(pard):
	if par == ".DS_Store":
		pass
	else:
		print("Opened " + par + " for formatting")
		with open(pard + par, 'r') as deparenned:
			fwr = [] # Wrapper for JSON
			lines = deparenned.readlines()
			for line in lines:
				if line == '\n':
					pass
				else:
					a = re.search(r'[A-Z]*:', line)
					if a is not None:
						a = a.group(0)[:-1] # Get actor
					# print(a)
						if a != "":
							d = re.search(r'\: (.*)$', line)
							if d is not None:
								d = d.group(0)[2:] # Get dialogue
					# print(d)
								fwr.append({'actor': a, 'dialogue': d})
			with open(ford + par[:-2] + 'fd', 'w+') as ff:
				ff.write(json.dumps(fwr))
				os.remove(pard + par)
				print("Successfully formatted " + par)
